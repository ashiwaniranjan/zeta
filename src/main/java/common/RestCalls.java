package common;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import java.util.HashMap;

/**
 * This class will responsible for maintain all Http Methods
 */


public class RestCalls {

    public static Response getRequest(String URI, HashMap hm) {
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.contentType(ContentType.JSON);
        requestSpecification.header("user-key",APIs.USER_KEY);
        requestSpecification.params(hm);
        Response response = requestSpecification.get(URI);
        return response;
    }
    public static Response getRequest(String URI,String USER_KEY) {
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.contentType(ContentType.JSON);
        requestSpecification.header("user-key",USER_KEY);
        Response response = requestSpecification.get(URI);
        return response;
    }
}
