package common;

public class APIs {

    public static final String USER_KEY="c10ad948c53d53a2d4a5464aedae2f6c";

    //Common
    public static final String CATEGORIES="https://developers.zomato.com/api/v2.1/categories";
    public static final String CITIES="https://developers.zomato.com/api/v2.1/cities";
    public static final String COLLECTIONS="https://developers.zomato.com/api/v2.1/collections";
    public static final String CUSINES="https://developers.zomato.com/api/v2.1/cusines";
    public static final String ESTABLISHMENTS="https://developers.zomato.com/api/v2.1/establishments";
    public static final String GEOCODE="https://developers.zomato.com/api/v2.1/geocode";

    //Location
    public static final String GETLOCATIONSDETAILS="https://developers.zomato.com/api/v2.1/location_details";
    public static final String GETLOCATIONS="https://developers.zomato.com/api/v2.1/location_details";

    //Restaurants
    public static final String DAILY_MENU="https://developers.zomato.com/api/v2.1//dailymenu";
    public static final String RESTAURANTS="https://developers.zomato.com/api/v2.1/restaurant";
    public static final String REVIEW="https://developers.zomato.com/api/v2.1/reviews";
    public static final String SEARCH="https://developers.zomato.com/api/v2.1/search";

}
