package common.dp;

import org.testng.annotations.DataProvider;

public class CommonDp {

    @DataProvider(name = "invaliduserid")
    public Object[][] getData() {
        return new Object[][]{
                {"abcde"},{"xysz"}
        };
    }
    @DataProvider(name = "getCity")
    public Object[][] getCity() {
        return new Object[][]{
                {"Patna"}
        };
    }

    @DataProvider(name = "getInvalidCity")
    public Object[][] getInCity() {
        return new Object[][]{
                {"xyzx"}
        };
    }
}
