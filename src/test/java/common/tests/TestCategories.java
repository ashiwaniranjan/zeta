package common.tests;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import common.APIs;
import common.RestCalls;
import common.dp.CommonDp;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Helper;



public class TestCategories {

    RestCalls rs=new RestCalls();
    Helper helper=new Helper();

    @Test(description = "This test will get categories with valid user id")
    public void testCollectionGet(){
        Response response=rs.getRequest(APIs.CATEGORIES,APIs.USER_KEY);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
        String cat_name= JsonPath.read(response.getBody().asString(),"$.categories[0].categories.name");
        System.out.println(cat_name);

    }

    @Test(description = "This test will get categories with invalid user id",dataProviderClass = CommonDp.class,dataProvider = "invaliduserid")
    public void testCollectionGetInvalidUserId(String user_id){
        Response response=rs.getRequest(APIs.CATEGORIES,user_id);
        Assert.assertEquals(403,response.getStatusCode());
    }
}
