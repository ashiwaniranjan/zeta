package common.tests;

import com.jayway.restassured.response.Response;
import common.APIs;
import common.RestCalls;
import common.dp.CommonDp;
import common.pojo.CityPojo;
import common.pojo.Location_suggestions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Helper;

import java.util.HashMap;

public class TestCollections {

    RestCalls rs=new RestCalls();
    Helper helper=new Helper();

    @Test(description = "This test will get collection details with valid city name",dataProviderClass = CommonDp.class,dataProvider = "getCity")
    public void testCollectionGet(String cityname){
        HashMap mp1=new HashMap();
        mp1.put("q",cityname);
        Response response=rs.getRequest(APIs.CITIES,mp1);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
        CityPojo pojo=helper.deserialize((response.getBody().asString()),CityPojo.class);
        String citi_id="";
        for(Location_suggestions location_suggestions:pojo.getLocation_suggestions())
            citi_id=location_suggestions.getId();
        HashMap mp=new HashMap();
        mp.put("city_id",Integer.parseInt(citi_id));
        Response responseget=rs.getRequest(APIs.COLLECTIONS,mp);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+responseget.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
    }

    @Test(description = "This test will get city details with invalid city name",dataProviderClass = CommonDp.class,dataProvider = "getInvalidCity")
    public void testCollectionsGetInvalidCityID(String cityname){
        HashMap mp=new HashMap();
        mp.put("city_id",cityname);
        Response response=rs.getRequest(APIs.COLLECTIONS,mp);
        Assert.assertEquals(400,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
    }

    @Test(description = "This test will get collections with invalid user id",dataProviderClass = CommonDp.class,dataProvider = "invaliduserid")
    public void testCollectionGetInvalidUserId(String user_id){
        Response response=rs.getRequest(APIs.COLLECTIONS,user_id);
        Assert.assertEquals(403,response.getStatusCode());
    }
}
