package common.tests;

import com.jayway.restassured.response.Response;
import common.APIs;
import common.RestCalls;
import common.dp.CommonDp;
import common.pojo.CityPojo;
import common.pojo.Location_suggestions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Helper;

import java.util.HashMap;

public class TestEstablishment {

    RestCalls rs=new RestCalls();
    Helper helper=new Helper();

    @Test(description = "This test will get establishments details with valid city name",dataProviderClass = CommonDp.class,dataProvider = "getCity")
    public void testEstablishmentsGet(String cityname){
        HashMap mp1=new HashMap();
        mp1.put("q",cityname);
        Response response=rs.getRequest(APIs.CITIES,mp1);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
        CityPojo pojo=helper.deserialize((response.getBody().asString()),CityPojo.class);
        String citi_id="";
        for(Location_suggestions location_suggestions:pojo.getLocation_suggestions())
            citi_id=location_suggestions.getId();
        HashMap mp=new HashMap();
        mp.put("city_id",Integer.parseInt(citi_id));
        Response responseget=rs.getRequest(APIs.ESTABLISHMENTS,mp);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+responseget.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
    }

    @Test(description = "This test will get establishments details with invalid city name",dataProviderClass = CommonDp.class,dataProvider = "getInvalidCity")
    public void testEstablishmentGetInvalidCityID(String cityname){
        HashMap mp=new HashMap();
        mp.put("city_id",cityname);
        Response response=rs.getRequest(APIs.ESTABLISHMENTS,mp);
        Assert.assertEquals(400,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
    }

    @Test(description = "This test will get /establishments with invalid user id",dataProviderClass = CommonDp.class,dataProvider = "invaliduserid")
    public void testEstablishmentGetInvalidUserId(String user_id){
        Response response=rs.getRequest(APIs.ESTABLISHMENTS,user_id);
        Assert.assertEquals(403,response.getStatusCode());
    }
}
