package common.tests;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import common.APIs;
import common.RestCalls;
import common.dp.CommonDp;
import common.pojo.CityPojo;
import common.pojo.Location_suggestions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Helper;

import java.util.HashMap;

public class TestCities {

    RestCalls rs=new RestCalls();
    Helper helper=new Helper();

    @Test(description = "This test will get city details with valid city name",dataProviderClass = CommonDp.class,dataProvider = "getCity")
    public void testCitiesGet(String cityname){
        HashMap mp=new HashMap();
        mp.put("q",cityname);
        Response response=rs.getRequest(APIs.CITIES,mp);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
        CityPojo pojo=helper.deserialize((response.getBody().asString()),CityPojo.class);
        for(Location_suggestions location_suggestions:pojo.getLocation_suggestions()){
            Assert.assertEquals(location_suggestions.getName(),cityname);
        }
    }

    @Test(description = "This test will get city details with invalid city name",dataProviderClass = CommonDp.class,dataProvider = "getInvalidCity")
    public void testCitiesGetInvalidName(String cityname){
        HashMap mp=new HashMap();
        mp.put("q",cityname);
        Response response=rs.getRequest(APIs.CITIES,mp);
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("Response "+response.getBody().asString());
        Assert.assertNotNull(response.getBody().asString());
        CityPojo pojo=helper.deserialize((response.getBody().asString()),CityPojo.class);
        Assert.assertEquals(pojo.getLocation_suggestions().length,0);
    }

    @Test(description = "This test will get citi with invalid user id",dataProviderClass = CommonDp.class,dataProvider = "invaliduserid")
    public void testCitiesGetInvalidUserId(String user_id){
        Response response=rs.getRequest(APIs.CITIES,user_id);
        Assert.assertEquals(403,response.getStatusCode());
    }
}
