package common.pojo;

public class CityPojo
{
    private String has_total;

    private Location_suggestions[] location_suggestions;

    private String has_more;

    private String user_has_addresses;

    private String status;

    public String getHas_total ()
    {
        return has_total;
    }

    public void setHas_total (String has_total)
    {
        this.has_total = has_total;
    }

    public Location_suggestions[] getLocation_suggestions ()
    {
        return location_suggestions;
    }

    public void setLocation_suggestions (Location_suggestions[] location_suggestions)
    {
        this.location_suggestions = location_suggestions;
    }

    public String getHas_more ()
    {
        return has_more;
    }

    public void setHas_more (String has_more)
    {
        this.has_more = has_more;
    }

    public String getUser_has_addresses ()
    {
        return user_has_addresses;
    }

    public void setUser_has_addresses (String user_has_addresses)
    {
        this.user_has_addresses = user_has_addresses;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [has_total = "+has_total+", location_suggestions = "+location_suggestions+", has_more = "+has_more+", user_has_addresses = "+user_has_addresses+", status = "+status+"]";
    }
}

