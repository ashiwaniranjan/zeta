package common.pojo;


public class Categories
{
    private String name;

    private String id;

    private Categories categories;

    public void setCategories(Categories categories){
        this.categories = categories;
    }
    public Categories getCategories(){
        return this.categories;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", id = "+id+"]";
    }
}
