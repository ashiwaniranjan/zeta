package common.pojo;

public class Location_suggestions
{
    private String should_experiment_with;

    private String has_new_ad_format;

    private String has_go_out_tab;

    private String discovery_enabled;

    private String is_state;

    private String state_name;

    private String name;

    private String country_name;

    private String country_flag_url;

    private String id;

    private String state_id;

    private String state_code;

    private String country_id;

    public String getShould_experiment_with ()
    {
        return should_experiment_with;
    }

    public void setShould_experiment_with (String should_experiment_with)
    {
        this.should_experiment_with = should_experiment_with;
    }

    public String getHas_new_ad_format ()
    {
        return has_new_ad_format;
    }

    public void setHas_new_ad_format (String has_new_ad_format)
    {
        this.has_new_ad_format = has_new_ad_format;
    }

    public String getHas_go_out_tab ()
    {
        return has_go_out_tab;
    }

    public void setHas_go_out_tab (String has_go_out_tab)
    {
        this.has_go_out_tab = has_go_out_tab;
    }

    public String getDiscovery_enabled ()
    {
        return discovery_enabled;
    }

    public void setDiscovery_enabled (String discovery_enabled)
    {
        this.discovery_enabled = discovery_enabled;
    }

    public String getIs_state ()
    {
        return is_state;
    }

    public void setIs_state (String is_state)
    {
        this.is_state = is_state;
    }

    public String getState_name ()
    {
        return state_name;
    }

    public void setState_name (String state_name)
    {
        this.state_name = state_name;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCountry_name ()
    {
        return country_name;
    }

    public void setCountry_name (String country_name)
    {
        this.country_name = country_name;
    }

    public String getCountry_flag_url ()
    {
        return country_flag_url;
    }

    public void setCountry_flag_url (String country_flag_url)
    {
        this.country_flag_url = country_flag_url;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getState_code ()
    {
        return state_code;
    }

    public void setState_code (String state_code)
    {
        this.state_code = state_code;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [should_experiment_with = "+should_experiment_with+", has_new_ad_format = "+has_new_ad_format+", has_go_out_tab = "+has_go_out_tab+", discovery_enabled = "+discovery_enabled+", is_state = "+is_state+", state_name = "+state_name+", name = "+name+", country_name = "+country_name+", country_flag_url = "+country_flag_url+", id = "+id+", state_id = "+state_id+", state_code = "+state_code+", country_id = "+country_id+"]";
    }
}
